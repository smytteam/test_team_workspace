import dynamoose from "dynamoose"; // eslint-disable-line

const memberSchema = new dynamoose.Schema({
    id: {
        type: String,
        hashKey: true
    },
    name: String,
    email: String,
    team: String
}, {
    timestamps: true
});

export const Member = dynamoose.model("Member", memberSchema);