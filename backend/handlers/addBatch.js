import {Member} from '../models/Member';
import uuid from 'uuid/v1';
import aws from 'aws-sdk';

const ses = new aws.SES();
const fromEmail = process.env.FROM_EMAIL;

export const addBatch = async ({body}, context, callback) => {
    const items = await Promise.all(addItems(body));
    items.map((member) => sendEmail(member));

    const response = {
        statusCode: 200,
        headers: {
            "Content-Type": "application/json",
            "Access-Control-Allow-Origin": "*"
        },
        body: JSON.stringify(items)
    };

    console.log(` => Items stored: ${items.length}`);
    callback(null, response)
};

const addItems = data => {
    console.log(data);
    const itemsData = JSON.parse(data).members;
    if (itemsData.length && typeof itemsData !== "string") {
        return itemsData.map(item => Member.create({
            id: uuid(),
            ...item
        }))
    } else {
        throw new Error("Type of items to add must be an array")
    }
};

const generateMemberURL = (member) => {
    return process.env.FRONTEND_URL + '/welcome'
};

const sendEmail = (member) => {
    const url = generateMemberURL(member);
    const params = {
        Source: fromEmail,
        Destination: {ToAddresses: [member.email]},
        ReplyToAddresses: [fromEmail],
        Message: {
            Body: {
                Text: {
                    Charset: "UTF-8",
                    Data: `Dear ${member.name}! Welcome to our team <a href="${url}">${url}</a>`
                }
            },
            Subject: {
                Charset: "UTF-8",
                Data: `Welcome to our team!`
            }
        }
    };
    ses.sendEmail(params).promise();
};