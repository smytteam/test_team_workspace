import {Member} from "../models/Member";

export const listAll = async ({body}, context, callback) => {
    const items = await Member.scan().exec();

    const response = {
        statusCode: 200,
        headers: {
            "Content-Type": "application/json",
            "Access-Control-Allow-Origin": "*"
        },
        body: JSON.stringify(items)
    };

    console.log(` => Items...
        ${items}`);
    callback(null, response)
};
