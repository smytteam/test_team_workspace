export default {
    apiGateway: {
        REGION: "us-east-1",
        URL: "https://jtib741da0.execute-api.eu-west-1.amazonaws.com/dev/",
    },
    workspace: {
        teamId: "c51949ca-88e4-11e9-bc42-526af7764f64"
    }
};
