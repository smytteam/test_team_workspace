import React from 'react';
import { BrowserRouter as Router, Route } from "react-router-dom";
import Home from "./components/Home";
import Welcome from "./components/Welcome";


function App() {
  return (
    <div className="App">
        <Router>
            <Route exact path="/" component={Home}/>
            <Route path="/welcome" component={Welcome}/>
        </Router>
    </div>
  );
}

export default App;
