import React from "react";
import {makeStyles} from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import {TableHead} from "@material-ui/core";


const useStyles = makeStyles(theme => ({
    root: {
        width: "100%",
        marginTop: theme.spacing(3),
    },
    table: {
        minWidth: 500,
    },
    tableWrapper: {
        overflowX: "auto",
    },
}));

function TeamMemberList(props) {
    const classes = useStyles();
    const {rows, loading} = props;

    function renderRows(rows) {
        return (
            rows.map(row => (
                <TableRow key={row.id}>
                    <TableCell>{row.name}</TableCell>
                    <TableCell>{row.email}</TableCell>
                </TableRow>
            ))
        )
    }

    function renderEmpty() {
        return (
            <TableRow>
                <TableCell colSpan={2} align={"center"}>
                    No members...
                </TableCell>
            </TableRow>
        )
    }

    function renderLoading() {
        return (
            <TableRow>
                <TableCell colSpan={2} align={"center"}>
                    Loading...
                </TableCell>
            </TableRow>
        )
    }

    return (
        <Paper className={classes.root}>
            <div className={classes.tableWrapper}>
                <Table className={classes.table}>
                    <TableHead>
                        <TableRow>
                            <TableCell component="th">Name</TableCell>
                            <TableCell component="th">Email</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {!loading && rows.length > 0 && renderRows(rows)}
                        {!loading && rows.length === 0 && renderEmpty()}
                        {loading && renderLoading()}
                    </TableBody>
                </Table>
            </div>
        </Paper>
    );
}

export default TeamMemberList;