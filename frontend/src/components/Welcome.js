import Header from "./Header";
import Container from "@material-ui/core/Container";
import React from "react";
import {makeStyles} from "@material-ui/core";


const useStyles = makeStyles(theme => ({
    message: {
        textAlign: "center",
        marginTop: 30 + "vh",

    }
}));

export default function Welcome() {
    const classes = useStyles();
    return (
        <div>
            <Header/>
            <Container maxWidth={"md"}>
                <h1 className={classes.message}>Congratulation, now your in the team!</h1>
            </Container>
        </div>
    )
}