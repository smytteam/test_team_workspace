import React, {useState} from "react";
import {makeStyles} from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import InviteMemberDialog from "./InviteMemberDialog";


const useStyles = makeStyles(theme => ({
    root: {
        flexGrow: 1,
    },
    title: {
        flexGrow: 1,
    },
}));

export default function Header(props) {
    const {onInviteSubmit} = props;
    const classes = useStyles();
    const [inviteDialogOpen, setInviteDialogOpen] = useState(false);

    function handleInvite() {
        setInviteDialogOpen(true);
    }

    function handleCloseDialog() {
        setInviteDialogOpen(false);
        onInviteSubmit();
    }

    return (
        <div className={classes.root}>
            <InviteMemberDialog open={inviteDialogOpen} onClose={handleCloseDialog}/>
            <AppBar position="static">
                <Toolbar>
                    <Typography variant="h6" className={classes.title}>
                        TeamWorkspace
                    </Typography>
                    <Button color="inherit" onClick={handleInvite}>Invite team</Button>
                </Toolbar>
            </AppBar>
        </div>
    );
}