import Header from "./Header";
import Container from "@material-ui/core/Container";
import TeamMembersList from "./TeamMembersList";
import React from "react";
import {API} from "aws-amplify";


export default function Home() {
    const [members, setMembers] = React.useState([]);
    const [loading, setLoading] = React.useState(false);

    function loadMembers() {
        setLoading(true);
        const fetchData = async () => {
            const members = await API.get("members", "api/list");
            setMembers(members);
            setLoading(false);
        };
        fetchData();
    }

    React.useEffect(loadMembers, []);

    return (
        <div>
            <Header onInviteSubmit={loadMembers}/>
            <Container maxWidth={"md"}>
                <TeamMembersList rows={members} loading={loading}/>
            </Container>
        </div>
    )
}