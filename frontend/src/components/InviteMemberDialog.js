import React, {useState} from "react";
import PropTypes from "prop-types";
import {makeStyles} from "@material-ui/core/styles";
import DialogTitle from "@material-ui/core/DialogTitle";
import Dialog from "@material-ui/core/Dialog";
import AddIcon from "@material-ui/icons/Add";
import FormControl from "@material-ui/core/FormControl";
import FormHelperText from "@material-ui/core/FormHelperText";
import InputLabel from "@material-ui/core/InputLabel";
import Input from "@material-ui/core/Input";
import Button from "@material-ui/core/Button";
import DialogContent from "@material-ui/core/DialogContent";
import DialogActions from "@material-ui/core/DialogActions";
import {API} from "aws-amplify";
import config from "../config";

const createMember = (email = "", name = "", teamId = config.workspace.teamId, dirty = true) => ({
    email,
    name,
    teamId,
    dirty
});

const useStyles = makeStyles(theme => ({
    input: {
        marginLeft: 10,
    },
    button: {
        margin: theme.spacing(1),
    },
    actions: {
        justifyContent: "flex-end",
        marginRight: 20,
        marginBottom: 20,
    },
    row: {
        marginBottom: 20,
    },
    formControl: {
        margin: theme.spacing(1),
    }
}));

function InviteMemberDialog(props) {
    const classes = useStyles();
    const [sending, setSending] = useState(false);
    const {onClose, ...other} = props;
    const [members, setMembers] = useState([createMember()]);

    function handleInviteMore() {
        setMembers(members.concat(createMember()));
    }

    function createMembers(body) {
        return API.post("members", "api/add/batch", {
            body: {members: body.members.map(({dirty, ...member}) => (member))}  // send all except dirty
        });
    }

    function handleChange(field, index) {
        return (event) => {
            setMembers(
                members.map((member, i) => {
                    if (index !== i) {
                        return member;
                    }
                    return {...member, [field]: event.target.value, dirty: false}
                })
            )
        }
    }

    async function handleSubmit(event) {
        event.preventDefault();
        try {
            setSending(true);
            await createMembers({members});
            setSending(false);
        } catch (e) {
            console.error(e);
        }

        // Clean members
        setMembers([createMember()]);
        onClose()
    }

    function validateName(member) {
        if (!member.dirty && member.name === "") {
            return "Is required"
        }
    }

    function validateEmail(member) {
        const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if (!member.dirty && !re.test(String(member.email).toLocaleLowerCase())) {
            return "Email is invalid";
        }
    }

    function isMembersValid() {
        return members.filter((member) => (member.dirty || validateName(member) || validateEmail(member))).length === 0;
    }

    return (
        <Dialog {...other}>
            <DialogTitle id="simple-dialog-title">Invite members</DialogTitle>
            <DialogContent>
                {members.map((member, index) => (
                    <div key={index} className={classes.row}>
                        <FormControl className={classes.formControl} error={!!(validateName(member))}>
                            <InputLabel htmlFor="component-error">Name</InputLabel>
                            <Input
                                value={member.name}
                                onChange={handleChange("name", index)}
                            />
                            {validateName(member) && (<FormHelperText>{validateName(member)}</FormHelperText>)}
                        </FormControl>
                        <FormControl className={classes.formControl} error={!!(validateEmail(member))}>
                            <InputLabel htmlFor="component-error">Email</InputLabel>
                            <Input
                                value={member.email}
                                onChange={handleChange("email", index)}
                            />
                            {validateEmail(member) && (<FormHelperText>{validateEmail(member)}</FormHelperText>)}
                        </FormControl>
                    </div>
                ))}
            </DialogContent>
            <DialogActions>
                <Button color="default"
                        className={classes.button}
                        onClick={handleInviteMore}>
                    <AddIcon/>
                    Invite more
                </Button>
                <Button variant="contained"
                        color="primary"
                        className={classes.button}
                        disabled={!isMembersValid() || sending}
                        onClick={handleSubmit}>
                    Submit
                </Button>
            </DialogActions>
        </Dialog>
    );
}

InviteMemberDialog.propTypes = {
    onClose: PropTypes.func,
    open: PropTypes.bool,
};

export default InviteMemberDialog;